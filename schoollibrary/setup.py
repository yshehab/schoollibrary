from setuptools import setup

setup(
    name='bpslibrary',
    package=['bpslibrary'],
    include_package_data=True,
    install_requires=[
        'bottlenose',
        'BeautifulSoup4',
        'flask',
        'flask_bcrypt',
        'flask_login',
        'flask_wtf',
        'numpy',
        'pillow',
        'pyisbn',
        'requests',
        'sqlalchemy',
        'uwsgi',
        'wtforms',
        'zbar_py'
    ],
)
