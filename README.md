# School Library

A stub for a primary school library system.

Initial objectives

- Aid the administration of the library thorough classification utilising online book databases
- Enhance the effectiveness of the library by being accessible, simple and easy to use
- Encourage use of library and help develop independent learning skills

Initial features

- Core
  - Add and remove books
  - Search for books using key terms (e.g. title, author, category...etc.)
  - Record and track loans
  - Provide information about library use (most popular books, category, author ...etc.)
- Enhanced
  - Book reviews
  - Next book recommendation
- Rich
  - Online access
  - Include audio books
